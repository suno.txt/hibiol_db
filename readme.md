# Hibiol DB

A small web extension for comparing performance between `IndexedDB`, `Dexie.js`, and `IndexedDB + Dexie.js` for [Hibiol](https://gitlab.com/suno.txt/hibiol).

## Using the extension

### Clone
```shell
git clone https://gitlab.com/suno.txt/hibiol_db.git hibiol_db
cd hibiol_db
npm install
```

### Config
```javascript
/*
	hibiol_db/src/index.js

	※ You might have to manually clear IndexedDB if an error pops up.

	Methods
	1	IndexedDB
	2	Dexie.js
	3	IndexedDB + Dexie.js
*/
const method = 1;
```

### Compile
```shell
npm run build
```

The extension is written to `hibiol_db/dist`.

### Compare

#### Firefox

1. Go to `about:debugging`.
2. Check "Enable add-on debugging".
3. Click "Load Temporary Add-on...".
4. Open `hibiol_db/dist/manifest.json` in the file manager prompt.
5. Click "Debug" in the extension's card.
6. Switch to the console.

#### Chromium and Friends

1. Go to the extensions page.
2. Enable "Developer mode".
3. Click "Load unpacked".
4. Open `hibiol_db/dist` in the file manager prompt.
5. Click `background.html` next to "Inspect views" in the extension's card.
6. Switch to the console.

Reload the extension to start another pass!