import Dexie from "dexie";

/*
	※ You might have to manually clear IndexedDB if an error pops up.

	Methods
	1	IndexedDB
	2	Dexie.js
	3	IndexedDB + Dexie.js
*/
const method = 1;

/*
	Start!
*/

var today_begin = new Date();
today_begin = new Date(today_begin.getFullYear(), today_begin.getMonth(), today_begin.getDate());
var today_end = new Date(today_begin.getFullYear(), today_begin.getMonth(), today_begin.getDate(), 23, 59, 59, 999);

var history_begin = new Date(today_begin);
history_begin.setDate(history_begin.getDate() - 3);
var history_end = new Date(today_end);
history_end.setDate(history_end.getDate() + 3);

var history = [];

for (let begin = new Date(history_begin); begin < history_end; /* 🌸 */) {
	let end = new Date(begin.getTime() + 7000);
	history.push({
		begin: begin,
		end: end,
		website: "sayoasa.jp"
	});
	begin = end;
}

if (method === 1) {
	(async () => {
		console.log("『 IndexedDB 』");

		// Open the Hibiol database.
		console.log("Opening the Hibiol database...");
		const Hibiol = await new Promise(resolve => {
			// Open the Hibiol database.
			let request = window.indexedDB.open("Hibiol", 1);

			// Upgrade the Hibiol database.
			request.addEventListener("upgradeneeded", event => {
				const Hibiol = event.target.result;

				/*
					Name [version]
				
					Store
						key				※
						index
							single		†
							multi		‡
							unique		†* or ‡*
						property
				*/

				/*
					Hibiol

					Events
						begin			※
						end				†*
						website			†
				*/
				
				if (event.oldVersion < 1) {
					// Events
					let Events = Hibiol.createObjectStore("Events", {keyPath: "begin"});
					Events.createIndex("end", "end", {unique: true});
					Events.createIndex("website", "website", {unique: false});
				}
			});

			// Return the opened Hibiol database.
			request.addEventListener("success", event => {
				resolve(event.target.result);
			});
		});

		// Memorize events.
		console.log("Memorizing " + history.length.toString() + " events...");
		await new Promise(resolve => {
			// Open a transaction.
			let transaction = Hibiol.transaction("Events", "readwrite");
			transaction.addEventListener("complete", event => {
				// Time.
				let time_end = new Date();
				console.log("Time:", time_end.getTime() - time_begin.getTime());

				resolve();
			});

			// Time.
			let time_begin = new Date();

			let Events = transaction.objectStore("Events");
			history.forEach(event => {
				Events.add(event);
			});
		});

		// Find events within today.
		console.log("Windowing events...");
		await new Promise(resolve => {
			// Open a transaction.
			let transaction = Hibiol.transaction("Events", "readonly");
			transaction.addEventListener("complete", event => {
				resolve();
			});

			// Time.
			let time_begin = new Date();

			let Events = transaction.objectStore("Events");
			let request = Events.openCursor(IDBKeyRange.upperBound(today_end), "prev");
			request.addEventListener("success", () => {
				// No cursor is returned if no matching records exist.
				let last_end = (event.target.result) ? event.target.result.value.end : today_end;

				// Find events within the window.
				let end = Events.index("end");
				let request = end.getAll(IDBKeyRange.bound(today_begin, last_end));
				request.addEventListener("success", event => {

					// Time.
					let time_end = new Date();
					console.log("Time:", time_end.getTime() - time_begin.getTime());

					// Display the result.
					let result = event.target.result;
					console.log("Events:", result.length);
					console.log(result[0]);
					console.log(result[1]);
					console.log("...");
					console.log(result[result.length - 2]);
					console.log(result[result.length - 1]);
					resolve();
				});
			});
		});

		// Delete the Hibiol database.
		console.log("Deleting the Hibiol database...");
		Hibiol.close();
		let request = window.indexedDB.deleteDatabase("Hibiol");
		request.addEventListener("success", event => {
			console.log("Done. (*•̀ᴗ•́*)و ̑̑");
		});
	})();
}

if (method === 2) {
	(async () => {
		console.log("『 Dexie.js 』");

		// Open the Hibiol database.
		console.log("Opening the Hibiol database...");
		const Hibiol = new Dexie("Hibiol");

		/*
			Hibiol

			Events
				begin			※
				end				†*
				website			†
		*/
		Hibiol.version(1).stores({
			Events: "begin, &end, website"
		});

		// Memorize events.
		console.log("Memorizing " + history.length.toString() + " events...");

		// Time.
		let time_begin = new Date();

		// Memorize events (cont'd).
		await Hibiol.Events.bulkAdd(history);

		// Time.
		let time_end = new Date();
		console.log("Time:", time_end.getTime() - time_begin.getTime());

		// Time.
		time_begin = new Date();

		// Find events within today.
		console.log("Windowing events...");
		let last_end = (await Hibiol.Events.where("begin").belowOrEqual(today_end).last()).end;
		let result = await Hibiol.Events.where("end")
			.between(today_begin, last_end, true, true)
			.toArray();

		// Time.
		time_end = new Date();
		console.log("Time:", time_end.getTime() - time_begin.getTime());

		// Display the result.
		console.log("Events:", result.length);
		console.log(result[0]);
		console.log(result[1]);
		console.log("...");
		console.log(result[result.length - 2]);
		console.log(result[result.length - 1]);

		// Delete the Hibiol database.
		console.log("Deleting the Hibiol database...");
		Hibiol.close();
		Hibiol.delete()
			.then(() => {
				console.log("Done. (*•̀ᴗ•́*)و ̑̑");
			});
	})();
}

if (method === 3) {
	(async () => {
		console.log("『 IndexedDB + Dexie.js 』");

		// Open the Hibiol database.
		console.log("Opening the Hibiol database...");

		/*
			Hibiol

			Events
				begin			※
				end				†*
				website			†
		*/
		const Hibiol = new Dexie("Hibiol");
		Hibiol.version(1).stores({
			Events: "begin, &end, website"
		});

		// Memorize events.
		console.log("Memorizing " + history.length.toString() + " events...");

		// Time.
		let time_begin = new Date();

		// Memorize events (cont'd).
		await Hibiol.Events.bulkAdd(history);

		// Time.
		let time_end = new Date();
		console.log("Time:", time_end.getTime() - time_begin.getTime());

		// Time.
		time_begin = new Date();

		// Find events within today.
		console.log("Windowing events...");
		await Hibiol.transaction("r", Hibiol.Events, dexie_transaction => {
			let transaction = dexie_transaction.idbtrans;
			let Events = transaction.objectStore("Events");
			let request = Events.openCursor(IDBKeyRange.upperBound(today_end), "prev");
			request.addEventListener("success", () => {
				// No cursor is returned if no matching records exist.
				let last_end = (event.target.result) ? event.target.result.value.end : today_end;

				// Find events within the window.
				let end = Events.index("end");
				let request = end.getAll(IDBKeyRange.bound(today_begin, last_end));
				request.addEventListener("success", event => {

					// Time.
					time_end = new Date();
					console.log("Time:", time_end.getTime() - time_begin.getTime());

					// Display the result.
					let result = event.target.result;
					console.log("Events:", result.length);
					console.log(result[0]);
					console.log(result[1]);
					console.log("...");
					console.log(result[result.length - 2]);
					console.log(result[result.length - 1]);
				});
			});
		});

		// Delete the Hibiol database.
		console.log("Deleting the Hibiol database...");
		Hibiol.close();
		Hibiol.delete()
			.then(() => {
				console.log("Done. (*•̀ᴗ•́*)و ̑̑");
			});
	})();
}